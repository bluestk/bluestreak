// the terminology(name of variables, and objects) is compatible with L293D driver schema.// the value is a number of the pin on the arduino Board, i my case Leonardo.co
const int ENGINE_LEFT_ENABLE_1 = 6;
const int ENGINE_LEFT_INPUT_1 = 12;
const int ENGINE_LEFT_INPUT_2 = 8;
int engineLeftSpeedPWM = 255;

const int ENGINE_RIGHT_ENABLE_2 = 5;
const int ENGINE_RIGHT_INPUT_3 = 4;
const int ENGINE_RIGHT_INPUT_4 = 2;
int engineRightSpeedPWM = 255;


//servo
#include <Servo.h> 
Servo myServo; 
int servoActualPosition = 0;
//

//ultrasonic sensor
const int TRIGGER = 10;
const int ECHO = 11;
//


void setup(){
   // setting up pin mode for the left engine.
   pinMode(ENGINE_LEFT_ENABLE_1, OUTPUT);
   pinMode(ENGINE_LEFT_INPUT_1, OUTPUT);
   pinMode(ENGINE_LEFT_INPUT_2, OUTPUT);

   // setting up pin mode for the right engine.
   pinMode(ENGINE_RIGHT_ENABLE_2, OUTPUT);
   pinMode(ENGINE_RIGHT_INPUT_3, OUTPUT);
   pinMode(ENGINE_RIGHT_INPUT_4, OUTPUT);
  
   Serial.begin(115200);     // opens serial port, sets data rate to 9600 bps
   while (Serial.available()>0); // only if you have leonardo.
   
   //setting up servo to pin 7;
   myServo.attach(7);
   
   //setting up pins for ultrasonic sensor
   pinMode(TRIGGER, OUTPUT);
   pinMode(ECHO, INPUT);
}

void loop(){
   serialEvent();
}

void movement(int leftEngineDirection, int leftEngineSpeed, int rightEngineDirection, int rightEngineSpeed){
   engineLeftSpeedPWM = leftEngineSpeed;
   engineRightSpeedPWM = rightEngineSpeed;
   
   //set up direction of left wheel
   if (leftEngineDirection == 0){
      digitalWrite(ENGINE_LEFT_INPUT_1,HIGH);
      digitalWrite(ENGINE_LEFT_INPUT_2,LOW);
   }
   else if (leftEngineDirection == 1){
      digitalWrite(ENGINE_LEFT_INPUT_1,LOW);
      digitalWrite(ENGINE_LEFT_INPUT_2,HIGH);
   }
   //set up direction of right wheel
   if (rightEngineDirection == 0){
     digitalWrite(ENGINE_RIGHT_INPUT_3,HIGH);
     digitalWrite(ENGINE_RIGHT_INPUT_4,LOW);
   }
   else if (rightEngineDirection == 1){
     digitalWrite(ENGINE_RIGHT_INPUT_3,LOW);
     digitalWrite(ENGINE_RIGHT_INPUT_4,HIGH);
   }
   //set up speed on left and righ wheel
   analogWrite(ENGINE_LEFT_ENABLE_1, engineLeftSpeedPWM );
   analogWrite(ENGINE_RIGHT_ENABLE_2, engineRightSpeedPWM);
   
}

void servoSetPosition(int newPosition, int servoStep, int delayBetweenSteps){
   if (newPosition >= servoActualPosition){
      for(int pos=servoActualPosition; pos<= newPosition; pos+=servoStep){
         myServo.write(pos);    // tell servo to go to position in variable 'pos' 
         delay(delayBetweenSteps); 
      }
   }
   else{
      for(int pos=servoActualPosition; pos>= newPosition; pos-=servoStep){
         myServo.write(pos);    // tell servo to go to position in variable 'pos' 
         delay(delayBetweenSteps); 
      }
   }
   servoActualPosition = newPosition;
}

void radar(int startPosition, int stopPosition, int servoStep, int delayBetweenSteps){
   if (startPosition < stopPosition){
      radarFromStartPositionToStopPosition(startPosition,stopPosition,servoStep,delayBetweenSteps);
      radarFromStopPositionToStartPosition(startPosition,stopPosition,servoStep,delayBetweenSteps);
   }
   else{
      int pom = startPosition;
      startPosition = stopPosition;
      stopPosition = pom;
      
      radarFromStopPositionToStartPosition(startPosition,stopPosition,servoStep,delayBetweenSteps);
      radarFromStartPositionToStopPosition(startPosition,stopPosition,servoStep,delayBetweenSteps);
   }
}
void radarFromStartPositionToStopPosition(int startPosition, int stopPosition, int servoStep, int delayBetweenSteps){
   for(int pos = startPosition; pos <= stopPosition; pos += servoStep){  // goes from startPosition degrees to stopPosition degrees  
      myServo.write(pos);    // tell servo to go to position in variable 'pos' 
      ultrasonicSensorGetDistance();
      if (checkSerialPort()){
         serialEvent();
      }
      delay(delayBetweenSteps); // waits delayBetweenStep ms for the servo to reach the position 
   }
}
void radarFromStopPositionToStartPosition(int startPosition, int stopPosition, int servoStep, int delayBetweenSteps){
   for(int pos = stopPosition; pos >= startPosition; pos -= servoStep){  // goes from startPosition degrees to stopPosition degrees  
      myServo.write(pos);    // tell servo to go to position in variable 'pos' 
      ultrasonicSensorGetDistance();
      if (checkSerialPort()){
         serialEvent();
      }
      delay(delayBetweenSteps); // waits delayBetweenStep ms for the servo to reach the position 
   }
}

boolean checkSerialPort(){
  if (Serial.available() > 0){
    return true;
  }
  else{
    return false;
  }
}

void ultrasonicSensorGetDistance(){
  long czas = 0;
  digitalWrite(TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER, LOW);
  czas = pulseIn(ECHO, HIGH);
  Serial.println(czas/58.0);
  delay(1);
}


void serialEvent(){
   String serialCommand = "";
   boolean isSerialCommand = false;
   
   // available() returns the available number of bytes;
   while (Serial.available() > 0){
      // read the incoming byte:
      int incomingByte = Serial.read();
    
      serialCommand += (char) incomingByte;
      if (Serial.available() == 0 ){
         isSerialCommand = true;
      }
      delay(5);
   }
   if (isSerialCommand){
      serialCommandActions(serialCommand);
      serialCommand = "";
   }
}

void serialCommandActions(String serialCommand){
   if (serialCommand.indexOf('(') > 0 && serialCommand.indexOf(')') > 0 && serialCommand.indexOf(';') > 0){
      String nameOfCommand = serialCommand.substring(0,serialCommand.indexOf('('));
      String params = serialCommand.substring(serialCommand.indexOf('(')+1,serialCommand.indexOf(')')) ;
      String paramsArray[getNumberOfParams(params)];
      getArrayOfParams(params,paramsArray);
      
      /*
      Serial.println(nameOfCommand);
      Serial.println(params);
      for(int i=0; i<getNumberOfParams(params); i++){
         Serial.print(paramsArray[i]);
      }
      */
      
      if (nameOfCommand.equalsIgnoreCase("M")){ // void movement(int leftEngineDirection, int leftEngineSpeed, int rightEngineDirection, int rightEngineSpeed)
         if (getNumberOfParams(params) == 4){
            movement(paramsArray[0].toInt(),paramsArray[1].toInt(),paramsArray[2].toInt(),paramsArray[3].toInt());
         }
      }
      if(nameOfCommand.equalsIgnoreCase("SP")){ // void servoSetPosition(int newPosition, int servoStep, int delayBetweenSteps)
         if (getNumberOfParams(params) == 3){
            servoSetPosition(paramsArray[0].toInt(),paramsArray[1].toInt(),paramsArray[2].toInt());
         }
      }
      if(nameOfCommand.equalsIgnoreCase("USSD")){ // void ultrasonicSensorGetDistance()
         if (getNumberOfParams(params) == 1){
            ultrasonicSensorGetDistance();
         }
      }
      if(nameOfCommand.equalsIgnoreCase("R")){ // void radar(int startPosition, int stopPosition, int servoStep, int delayBetweenSteps)
         if (getNumberOfParams(params) == 4){
            radar(paramsArray[0].toInt(),paramsArray[1].toInt(),paramsArray[2].toInt(),paramsArray[3].toInt());
         }
      } 
   }
   else{
      Serial.println("bad command");
   }
}

void getArrayOfParams(String params, String *results){
   int pStart = 0;
   int pStop = 0;
   int counter =0;
   for (int i=0; i<params.length(); i++){
      if (params.charAt(i) == ','){
         pStop=i;
         results[counter] = params.substring(pStart,pStop);
         counter++;
         pStart=i+1;
      }
   }
   results[getNumberOfParams(params)-1] = params.substring(pStart,params.length());
}

int getNumberOfParams(String params){
   int i=0;
   int commaCounter = 0;
   for (int i=0; i<params.length(); i++){
      if (params.charAt(i) == ','){
         commaCounter+=1;
      }
   }
   return commaCounter+1;
}






