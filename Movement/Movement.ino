// the terminology(name of variables, and objects) is compatible with L293D driver schema.
// the value is a number of the pin on the arduino Board, i my case Leonardo.co
const int ENGINE_LEFT_ENABLE_1 = 6;
const int ENGINE_LEFT_INPUT_1 = 12;
const int ENGINE_LEFT_INPUT_2 = 8;

const int ENGINE_RIGHT_ENABLE_2 = 5;
const int ENGINE_RIGHT_INPUT_3 = 4;
const int ENGINE_RIGHT_INPUT_4 = 2;

int engineSpeedPWM = 255; // values from 0 to 255;
String direction = "forward"; // forward/backward

int incomingByte = 0;   // for incoming serial data
String* serialCommand = new String("");
boolean isSerialCommand  = false;

//servo
#include <Servo.h> 
Servo myservo; 
int pos = 0;
//

//ultrasonic sensor
const int TRIGGER = 10;
const int ECHO = 11;
//

void setup(){
  // setting up pin mode for the left engine.
  pinMode(ENGINE_LEFT_ENABLE_1, OUTPUT);
  pinMode(ENGINE_LEFT_INPUT_1, OUTPUT);
  pinMode(ENGINE_LEFT_INPUT_2, OUTPUT);

  // setting up pin mode for the right engine.
  pinMode(ENGINE_RIGHT_ENABLE_2, OUTPUT);
  pinMode(ENGINE_RIGHT_INPUT_3, OUTPUT);
  pinMode(ENGINE_RIGHT_INPUT_4, OUTPUT);

  Serial.begin(9600);     // opens serial port, sets data rate to 9600 bps
  
  myservo.attach(7);
  
  pinMode(TRIGGER, OUTPUT);
  pinMode(ECHO, INPUT);
  
  //while (Serial.available()); // only if you have leonardo.
}

void loop(){
  serialEvent();
  radar();
}

void test(){
  forward();
  delay(1000);
  backward();
  delay(1000);
  left();
  delay(1000);
  right();
  delay(1000);
}

void drive(){
  analogWrite(ENGINE_LEFT_ENABLE_1, engineSpeedPWM);
  analogWrite(ENGINE_LEFT_ENABLE_1, engineSpeedPWM);

  if (direction.equalsIgnoreCase("forward")){
    digitalWrite(ENGINE_LEFT_INPUT_1,LOW);
    digitalWrite(ENGINE_LEFT_INPUT_2,HIGH);

    analogWrite(ENGINE_RIGHT_ENABLE_2, engineSpeedPWM);
    digitalWrite(ENGINE_RIGHT_INPUT_3,LOW);
    digitalWrite(ENGINE_RIGHT_INPUT_4,HIGH);
  }
  if (direction.equalsIgnoreCase("backward")){  
    digitalWrite(ENGINE_LEFT_INPUT_1,HIGH);
    digitalWrite(ENGINE_LEFT_INPUT_2,LOW);

    analogWrite(ENGINE_RIGHT_ENABLE_2, engineSpeedPWM);
    digitalWrite(ENGINE_RIGHT_INPUT_3,HIGH);
    digitalWrite(ENGINE_RIGHT_INPUT_4,LOW);
  }
}


void forward(){
  direction = "forward";
  analogWrite(ENGINE_LEFT_ENABLE_1, engineSpeedPWM);
  digitalWrite(ENGINE_LEFT_INPUT_1,LOW);
  digitalWrite(ENGINE_LEFT_INPUT_2,HIGH);

  analogWrite(ENGINE_RIGHT_ENABLE_2, engineSpeedPWM);
  digitalWrite(ENGINE_RIGHT_INPUT_3,LOW);
  digitalWrite(ENGINE_RIGHT_INPUT_4,HIGH);
}

void backward(){
  direction = "backward";
  analogWrite(ENGINE_LEFT_ENABLE_1, engineSpeedPWM);
  digitalWrite(ENGINE_LEFT_INPUT_1,HIGH);
  digitalWrite(ENGINE_LEFT_INPUT_2,LOW);

  analogWrite(ENGINE_RIGHT_ENABLE_2, engineSpeedPWM);
  digitalWrite(ENGINE_RIGHT_INPUT_3,HIGH);
  digitalWrite(ENGINE_RIGHT_INPUT_4,LOW);
}

void left(){

  analogWrite(ENGINE_LEFT_ENABLE_1, 0);
  digitalWrite(ENGINE_LEFT_INPUT_1,LOW);
  digitalWrite(ENGINE_LEFT_INPUT_2,HIGH);

  analogWrite(ENGINE_RIGHT_ENABLE_2, engineSpeedPWM);
  digitalWrite(ENGINE_RIGHT_INPUT_3,LOW);
  digitalWrite(ENGINE_RIGHT_INPUT_4,HIGH);
}

void right(){
  analogWrite(ENGINE_LEFT_ENABLE_1, engineSpeedPWM);
  digitalWrite(ENGINE_LEFT_INPUT_1,LOW);
  digitalWrite(ENGINE_LEFT_INPUT_2,HIGH);

  analogWrite(ENGINE_RIGHT_ENABLE_2, 0);
  digitalWrite(ENGINE_RIGHT_INPUT_3,LOW);
  digitalWrite(ENGINE_RIGHT_INPUT_4,HIGH);
}

void stop(){
  analogWrite(ENGINE_LEFT_ENABLE_1, 0);
  analogWrite(ENGINE_RIGHT_ENABLE_2, 0);
}

void serialEvent(){
  // available() returns the available number of bytes;
  while (Serial.available() > 0){
    // read the incoming byte:
    incomingByte = Serial.read();

    // print a received bit in decimal way
    Serial.print("I received: ");
    Serial.println(incomingByte, DEC);
    *serialCommand += (char) incomingByte;
    if (Serial.available() == 0 ){
      isSerialCommand = true;
    }
    delay(10);
  }
  if (isSerialCommand == true){
    Serial.println(*serialCommand);
    if (serialCommand->equalsIgnoreCase("f")){
      forward();
    }
    if (serialCommand->equalsIgnoreCase("b")){
      backward();
    }
    if (serialCommand->equalsIgnoreCase("l")){
      left();
    }
    if (serialCommand->equalsIgnoreCase("r")){
      right();
    }
    if (serialCommand->equalsIgnoreCase("s")){
      stop();
    }
    
    isSerialCommand = false;
    serialCommand = new String("");
  };

}

void radar(){
  servoMovement();
}

void servoMovement(){
  for(pos = 0; pos < 180; pos += 5)  // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    myservo.write(pos);    // tell servo to go to position in variable 'pos' 
    ultrasonicSensorActions();
    if (checkSerialPort()){
      serialEvent();
    }
    delay(50);                       // waits 15ms for the servo to reach the position 
  } 
  for(pos = 180; pos>=1; pos-=5)     // goes from 180 degrees to 0 degrees 
  {                                
    myservo.write(pos);    // tell servo to go to position in variable 'pos' 
    ultrasonicSensorActions();
    if (checkSerialPort()){
      serialEvent();
    };
    delay(50);                       // waits 15ms for the servo to reach the position 
  }
}

void ultrasonicSensorActions(){
  long czas = 0;
  digitalWrite(TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER, LOW);
  czas = pulseIn(ECHO, HIGH);
  Serial.println(czas/58.0);
  
  delay(1);
}

boolean checkSerialPort(){
  if (Serial.available() > 0){
    return true;
  }
  else{
    return false;
  }
}





