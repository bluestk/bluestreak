#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <SDL2/SDL.h>
#include <string.h>
#include <QSettings>
#include "../logger/logger.h"
#include "../navigation/robotnavigation.h"

class Controller
{

private:

    bool left_wheel_direction = 1;
    int left_wheel_power = 0;
    bool right_wheel_direction = 1;
    int right_wheel_power = 0;

    int current_gear = 2;

    int gear_up;
    int gear_down;

    int pressedKey;

    Logger* logger;

    SDL_Joystick * joy = NULL;

    void log(string msg);
    void updateKeySettings();


public:
    Controller();
    ~Controller();


    void gamepadCheckPressedButtons();
    void gamepadCheckPressedHat();
    bool gamepadIsConnected();
    void sendMovementCommand(RobotNavigation *robot_navigation);
    void setLogger(Logger* l) {
        logger = l;
    }
};

#endif // CONTROLLER_H
