#ifndef CONTROLLERWIDGET_H
#define CONTROLLERWIDGET_H

#include <QWidget>
#include "controller.h"
#include "../navigation/robotnavigation.h"
#include <QTimer>

namespace Ui {
class ControllerWidget;
}

class ControllerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ControllerWidget(QWidget *parent = 0);
    ~ControllerWidget();
    void setRobotNavigation(RobotNavigation *robot_navigation);

private slots:
    void update();
private:

    Ui::ControllerWidget *ui;
    Controller *controller = NULL;
    RobotNavigation *robot_navigation = NULL;
    QTimer *timer = NULL;
};

#endif // CONTROLLERWIDGET_H
