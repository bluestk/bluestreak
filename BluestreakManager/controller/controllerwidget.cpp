#include "controllerwidget.h"
#include "ui_controllerwidget.h"

ControllerWidget::ControllerWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ControllerWidget)
{
    ui->setupUi(this);
    controller = new Controller();
    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    if(controller->gamepadIsConnected()) {
        timer->start(100);
        ui->status->setText("Connected");
    } else {
        ui->status->setText("Disconnected");
    }
    this->setFocus();
}

ControllerWidget::~ControllerWidget()
{
    timer->stop();
    delete timer;
    delete controller;
    delete ui;
}

void ControllerWidget::setRobotNavigation(RobotNavigation *robot_navigation)
{
    this->robot_navigation = robot_navigation;
}

void ControllerWidget::update()
{
    if(controller->gamepadIsConnected()) {
        controller->gamepadCheckPressedHat();
        controller->gamepadCheckPressedButtons();
        controller->sendMovementCommand(robot_navigation);
    }
}
