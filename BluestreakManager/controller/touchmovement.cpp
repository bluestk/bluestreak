#include "touchmovement.h"
#include <iostream>
#include <QMouseEvent>
#include <QTime>
#include <QPaintEvent>
#include <QPainter>
#include <cmath>

TouchMovement::TouchMovement(QWidget *parent) :
    QWidget(parent), isMouseDown(false), isMoving(false), logger(NULL)
{
    lastMoveHandled.start();
}

void TouchMovement::log(string msg) {
    if( logger == NULL )
        return;
    logger->logMovement(msg);
}

void TouchMovement::mousePressEvent(QMouseEvent * event) {
    if( event->button() != Qt::MouseButton::LeftButton ) {
        return;
    }
    isMouseDown = true;
    emit mousePressChange(true);
    log("Mouse press!");
    handleMove(event);
}

void TouchMovement::mouseReleaseEvent(QMouseEvent * event) {
    if( event->button() != Qt::MouseButton::LeftButton ) {
        return;
    }
    isMouseDown = false;
    emit mousePressChange(false);
    log("Mouse release!");
}

void TouchMovement::mouseMoveEvent(QMouseEvent * event) {
    if( !isMouseDown || isMoving )
        return;
    if( lastMoveHandled.elapsed() < moveDelayMs ) {
        return;
    }
    isMoving = true;

    handleMove(event);

    isMoving = false;
    lastMoveHandled.restart();
}

void TouchMovement::paintEvent(QPaintEvent * event) {
    QRect rectToDraw;
    int eWidth = size().width() - 5; // -5 padding ; )
    int eHeight = size().height() - 5;

    if( eWidth > eHeight ) {
        rectToDraw.setWidth( eHeight );
        rectToDraw.setHeight( eHeight );
        circleR = eHeight / 2;
        int x = ( size().width() - eHeight ) / 2;
        rectToDraw.setCoords(x, 0, x+eHeight, eHeight );
    } else {
        rectToDraw.setWidth( eWidth );
        rectToDraw.setHeight( eWidth );
        circleR = eWidth / 2;
        int y = (size().height() - eWidth) / 2;
        rectToDraw.setCoords(0, y, eWidth, y+eWidth );
    }
    xCenter = rectToDraw.x() +  ( rectToDraw.width() / 2 );
    yCenter = rectToDraw.y() +  ( rectToDraw.height() / 2 );

    QPainter painter(this);
    painter.drawEllipse(rectToDraw);
    // Horizontal line
    painter.drawLine(rectToDraw.x(), yCenter, rectToDraw.x()+rectToDraw.width(), yCenter);
    // Vertical line
    painter.drawLine(xCenter, rectToDraw.y(), xCenter, rectToDraw.y()+rectToDraw.height());
    // Cross lines
    static const double rad = 45 * M_PI / 180;
    int r = ( rectToDraw.width() / 2 );
    painter.drawLine(xCenter - r * sin(rad),
                     yCenter - r * cos(rad),
                     xCenter + r * sin(rad),
                     yCenter + r * cos(rad));
    painter.drawLine(xCenter - r * sin(rad),
                     yCenter + r * cos(rad),
                     xCenter + r * sin(rad),
                     yCenter - r * cos(rad));



    painter.setBrush(Qt::red);
    painter.drawEllipse(xCenter-5, yCenter-5, 10, 10);
}

void TouchMovement::handleMove(QMouseEvent *event) {
    // Punkty w kole
    // Miara łukowa
    // pomocnicza
    // kąt w stopniach
    static double x, y, radAngle, z, angle;

    x = event->x() - xCenter;
    y = yCenter - event->y();
    if ( x == 0 ) {
        if( x >= 0 ) {
            radAngle = M_PI / 2;
        } else {
            radAngle = 3* M_PI / 2;
        }
    } else {
        z = atan( y / x );
        if( x > 0 && y > 0 )
            radAngle = z;
        else if ( y >= 0 && x < 0 )
            radAngle = M_PI + z;
        else if ( y < 0 && x < 0 )
            radAngle = M_PI + z;
        else
            radAngle = 2*M_PI + z;
    }
    angle = radAngle * 180 / M_PI;

    // Patrz definicja odległości. Metryka euklidesowa
    int power = sqrt( pow( event->x()-xCenter, 2 ) + pow( event->y()-yCenter, 2) );
    power *= 100;
    power /= circleR;
    power = min(power, 100);

    string msg = "Move! Moc: ";
    msg += std::to_string( power );
    msg += " kąt: ";
    msg += std::to_string(angle);
    log(msg);
    emit mouseMove( angle, power );
}
