#ifndef TOUCHMOVEMENT_H
#define TOUCHMOVEMENT_H
#include <QMouseEvent>
#include <QWidget>
#include <QTime>
#include "../logger/logger.h"

class TouchMovement : public QWidget
{
    Q_OBJECT
    bool isMouseDown;
    bool isMoving;
    int xCenter;
    int yCenter;
    int circleR;
    int moveDelayMs = 250;
    QTime lastMoveHandled;
    Logger* logger;

    void log(string msg);
    void handleMove(QMouseEvent* event);

protected:
    void mousePressEvent(QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent * event);
    void paintEvent(QPaintEvent * event);

public:
    explicit TouchMovement(QWidget *parent = 0);
    void setLogger(Logger* l) {
        logger = l;
    }

signals:
    void mousePressChange(bool pressed);
    // Angle - kat wzgledem dodatniej czesci osi OX
    // Power - 0-100 moc silnikow; relatywnie procent odległości od środka koła
    void mouseMove(double angle, int power);

};

#endif // TOUCHMOVEMENT_H
