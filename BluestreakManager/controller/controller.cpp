#include "controller.h"

void Controller::log(string msg)
{
    if( logger == NULL )
        return;
    logger->logMovement(msg);
}

Controller::Controller()
{
    updateKeySettings();
    if (SDL_NumJoysticks() > 0) {
        joy = SDL_JoystickOpen(0);
    }
}

void Controller::updateKeySettings()
{
    QSettings settings;
    gear_up   = settings.value("controller/gear_up", 0).toInt();
    gear_down   = settings.value("controller/gear_down", 0).toInt();
}

void Controller::gamepadCheckPressedButtons()
{
    SDL_JoystickUpdate();
    bool buttonPressed;
    buttonPressed = SDL_JoystickGetButton(joy, gear_up);
    if(buttonPressed && current_gear < 2) {
        current_gear++;
    } else {
        buttonPressed = SDL_JoystickGetButton(joy, gear_down);
        if(buttonPressed && current_gear > 1) {
            current_gear--;
        }
    }

    switch( current_gear ) {
    case 2:
      break;
    case 1:
        left_wheel_power  -= left_wheel_power/4;
        right_wheel_power -= right_wheel_power/4;
      break;
    }
}

void Controller::gamepadCheckPressedHat()
{
    SDL_JoystickUpdate();
    uint8_t hat_value = SDL_JoystickGetHat(joy, 0);
    switch (hat_value) {
    case SDL_HAT_CENTERED:
        right_wheel_direction = 1;
        left_wheel_direction = 1;
        left_wheel_power = 0;
        right_wheel_power = 0;
      break;
    case SDL_HAT_UP:
        right_wheel_direction = 1;
        left_wheel_direction = 1;
        left_wheel_power = 255;
        right_wheel_power  = 255;
      break;
    case SDL_HAT_DOWN:
        right_wheel_direction = 0;
        left_wheel_direction = 0;
        left_wheel_power = 255;
        right_wheel_power = 255;
      break;
    case SDL_HAT_LEFT:
        right_wheel_direction = 1;
        left_wheel_direction = 0;
        left_wheel_power = 255;
        right_wheel_power = 255;
      break;
    case SDL_HAT_RIGHT:
        right_wheel_direction = 0;
        left_wheel_direction = 1;
        left_wheel_power = 255;
        right_wheel_power = 255;
      break;
    case SDL_HAT_LEFTUP:
        right_wheel_direction = 1;
        left_wheel_direction = 1;
        left_wheel_power = 128;
        right_wheel_power = 255;
      break;
    case SDL_HAT_RIGHTUP:
        right_wheel_direction = 1;
        left_wheel_direction = 1;
        left_wheel_power = 255;
        right_wheel_power = 128;
      break;
    case SDL_HAT_LEFTDOWN:
        right_wheel_direction = 0;
        left_wheel_direction = 0;
        left_wheel_power = 128;
        right_wheel_power = 255;
      break;
    case SDL_HAT_RIGHTDOWN:
        right_wheel_direction = 0;
        left_wheel_direction = 0;
        left_wheel_power = 255;
        right_wheel_power = 128;
      break;
    default:
      break;
    }
}

bool Controller::gamepadIsConnected()
{
    if(joy == NULL) {
        return false;
    }
    return true;
}

void Controller::sendMovementCommand(RobotNavigation *robot_navigation)
{
    if(robot_navigation != NULL) {
        robot_navigation->sendMovementCommand(left_wheel_direction,left_wheel_power,right_wheel_direction,right_wheel_power);
    }
}

Controller::~Controller()
{
        if (SDL_JoystickGetAttached(joy)) {
            SDL_JoystickClose(joy);
        }
}

