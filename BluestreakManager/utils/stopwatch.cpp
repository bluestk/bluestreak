#include "stopwatch.h"

Stopwatch::Stopwatch()
{
    start();
}

Stopwatch::~Stopwatch()
{
    //dtor
}

void Stopwatch::start()
{
     clock_start = clock_end = std::chrono::high_resolution_clock::now();
}

void Stopwatch::stop()
{
    clock_end = std::chrono::high_resolution_clock::now();
}

std::chrono::milliseconds Stopwatch::elapsed()
{
    std::chrono::milliseconds delta = std::chrono::duration_cast<std::chrono::milliseconds>(clock_end-clock_start);
    return delta;
}
