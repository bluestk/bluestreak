#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <chrono>


class Stopwatch
{
    public:
        Stopwatch();
        ~Stopwatch();
        void start();
        void stop();
        std::chrono::milliseconds elapsed();
    private:
        std::chrono::time_point<std::chrono::high_resolution_clock> clock_start, clock_end;
};

#endif // STOPWATCH_H
