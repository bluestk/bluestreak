#ifndef QWIDGETWITHRADAR_H
#define QWIDGETWITHRADAR_H

#include <QWidget>
#include <QPainter>
#include <QPaintEvent>
#include "radar.h"

class QWidgetWithRadar : public QWidget
{
    Q_OBJECT
public:
    QWidgetWithRadar(QWidget* parent = 0);
    ~QWidgetWithRadar();
public:
    void paintEvent(QPaintEvent * event) Q_DECL_OVERRIDE;
    void setAngleLineOnRadar(float angleOfLineToXAxis, float lengthOfLine);
private:
    Radar* radar;
};

#endif // QWIDGETWITHRADAR_H
