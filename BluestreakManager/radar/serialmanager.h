#ifndef SERIALMANAGER_H
#define SERIALMANAGER_H

#include <QSettings>
#include <QBasicTimer>
#include "../lib/serialconnection.h"
#include <QTimerEvent>
#include <QTimer>

class SerialManager
{
public:
    SerialManager(int intervalTime);
protected:
    void timerEvent(QTimerEvent* event);
private:
    SerialConnection* serial_connection;
    QBasicTimer basicTimer;
};

#endif // SERIALMANAGER_H
