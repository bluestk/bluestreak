#ifndef RADAR_H
#define RADAR_H

#include <QPainter>
#include <QObject>
#include <list>

class Radar : public QObject
{
    Q_OBJECT
public:
    Radar(int QWidgetWidth, int QWidgetHeight, QObject* parent = 0);
    ~Radar();
public:
    void paint(QPainter* painter);
public:
    void drawTestRadar(QPainter *painter);
    void drawRadar();
    void drawCirclesAndCordinatesOfRadar();
    void drawAngleLineOnRadar();
    void drawAngleLineOnRadar(QLineF* angleLine);
    void drawAngleLineOnRadar(float angleOfLineToXAxis, float lengthOfLine);
    void drawSecondPartOfAngleLine(QLineF* angleLine);
    void drawListOfAngleLines();
private:
    void drawTestLinesOnRadar();
private:
    QPainter* painter;
    QPen* pen;
    QPointF* centerOfTheRadar;
    float radiusOfRadar;
    int numberOfCirclesInRadar;
    float spaceBetweenCirclesInRadar;
    QLineF angleLine;
    std::list <QLineF*> listOfAngleLines;
public:
    void refreshRadarData(int QWidgetWidth, int QWidgetHeight);
    void setPainter(QPainter* painter);
    void setPen(QColor color, Qt::PenStyle style, int width);
    void setCenterOfTheRadar(QPointF *point);
    void setNumberOfCirclesInRadar(int numberOfCirclesInRadar);
    void setAngleLine(float angleOfLineToXAxis, float lengthOfLine);
    void setAngleLine(QLineF* angleLine, float angleOfLineToXAxis, float lengthOfLine);
};

#endif // RADAR_H
