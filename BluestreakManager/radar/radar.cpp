
#include "radar.h"
#include <QLineF>
#include <iterator>

Radar::Radar(int QWidgetWidth, int QWidgetHeight, QObject* parent) : QObject(parent = 0)
{
    this->painter = NULL;
    this->pen = NULL;
    this->centerOfTheRadar = NULL;
    this->radiusOfRadar = 0.0;
    this->numberOfCirclesInRadar = 4;
    this->spaceBetweenCirclesInRadar = 0.0;
    this->refreshRadarData(QWidgetWidth, QWidgetHeight);
    this->setAngleLine(10,200);
}

Radar::~Radar(){
    //delete this->painter; -- tego nie usuwam poniewaz nie przydzialam pamieci do tego wskaznika;
    delete this->pen;
    delete this->centerOfTheRadar;
}

void Radar::paint(QPainter *painter){
    this->painter = painter;
    this->drawRadar();
}

void Radar::drawTestRadar(QPainter *painter){
    //drawing
    this->painter = painter;
    this->drawCirclesAndCordinatesOfRadar();
    this->drawTestLinesOnRadar();
    //
    QString s = QString();
    s.setNum(this->radiusOfRadar);
    this->painter->drawText(0,0,100,100,0,s,NULL);
}

void  Radar::drawTestLinesOnRadar(){
    int sizeOfArrayOfLineF = 30;
    QLineF** arrayOfQLineF = new QLineF*[sizeOfArrayOfLineF];

    this->setPen(Qt::black,Qt::SolidLine,2);
    qreal rx = centerOfTheRadar->x();
    qreal ry = centerOfTheRadar->y();
    QPointF centerPoint(rx,ry);

    for (int i=0; i<sizeOfArrayOfLineF; i++){
        arrayOfQLineF[i] = new QLineF();
        arrayOfQLineF[i]->setP1(QPointF(centerPoint));
        srand (time(NULL));
        arrayOfQLineF[i]->setAngle(rand() % 360 +i);

        int lengthOfline = rand() % (int)radiusOfRadar;
        arrayOfQLineF[i]->setLength(lengthOfline);
        painter->drawLine(*(arrayOfQLineF[i]));
    }

    for (int i=0; i<sizeOfArrayOfLineF; i++){
        delete arrayOfQLineF[i];
    }

}

void Radar::drawRadar(){
    //drawing
    this->drawCirclesAndCordinatesOfRadar();
    //
    QString s = QString();
    s.setNum(this->radiusOfRadar);
    this->painter->drawText(0,0,100,100,0,s,NULL);
}

void Radar::drawCirclesAndCordinatesOfRadar(){
    this->setPen(Qt::darkGreen,Qt::SolidLine,1);
    // drawing a circles;
    for (int i=0; i<this->numberOfCirclesInRadar; i++){
        double radiusOfNextCircle = this->radiusOfRadar - (this->spaceBetweenCirclesInRadar * i);
        this->painter->drawEllipse(*(this->centerOfTheRadar),qreal(radiusOfNextCircle), qreal(radiusOfNextCircle));
    }
    //drawing coorginate system;
    painter->drawLine(centerOfTheRadar->x() - radiusOfRadar, centerOfTheRadar->y(),
                      centerOfTheRadar->x() + radiusOfRadar, centerOfTheRadar->y());
    painter->drawLine(centerOfTheRadar->x(),centerOfTheRadar->y() - radiusOfRadar,
                      centerOfTheRadar->x(), centerOfTheRadar->y() + radiusOfRadar);
}

void Radar::drawAngleLineOnRadar(float angleOfLineToXAxis, float lengthOfLine){
    this->setPen(Qt::red,Qt::SolidLine,1);
    this->setAngleLine(angleOfLineToXAxis, lengthOfLine);
    this->painter->drawLine(this->angleLine);
    this->drawSecondPartOfAngleLine(&(this->angleLine));
}
void Radar::drawSecondPartOfAngleLine(QLineF* angleLine){
    this->setPen(Qt::black,Qt::SolidLine,2);
    QLineF secondPartOfAngleLine;
    secondPartOfAngleLine.setP1(angleLine->p2());
    secondPartOfAngleLine.setAngle(angleLine->angle());
    secondPartOfAngleLine.setLength(this->radiusOfRadar);
    this->painter->drawLine(secondPartOfAngleLine);
}

void Radar::drawAngleLineOnRadar(){
    this->setPen(Qt::red,Qt::SolidLine,1);
    this->setAngleLine(this->angleLine.angle(), this->angleLine.length());
    painter->drawLine(this->angleLine);
    this->drawSecondPartOfAngleLine(&(this->angleLine));
}

void Radar::drawAngleLineOnRadar(QLineF* angleLine){
    this->setPen(Qt::red,Qt::SolidLine,2);
    painter->drawLine(*angleLine);
    this->drawSecondPartOfAngleLine(angleLine);
}

void Radar::drawListOfAngleLines(){
    int sizeOfList = 30;
    if (this->listOfAngleLines.size() == sizeOfList){
        delete this->listOfAngleLines.front();
        this->listOfAngleLines.pop_front();
    }
    QLineF* newAngleLine = new QLineF();
    newAngleLine->setP1(this->angleLine.p1());
    newAngleLine->setAngle(this->angleLine.angle());
    newAngleLine->setLength(this->angleLine.length());

    this->listOfAngleLines.push_back(newAngleLine);

    std::list<QLineF*>::iterator iteratorOfList;

    for (iteratorOfList = this->listOfAngleLines.begin(); iteratorOfList != this->listOfAngleLines.end(); iteratorOfList++){
        this->drawAngleLineOnRadar(&(**iteratorOfList));
    }

}

void Radar::refreshRadarData(int QWidgetWidth, int QWidgetHeight){
    // collecting important data
    if (QWidgetHeight > QWidgetWidth){
        this->radiusOfRadar = QWidgetWidth;
    }else{
        this->radiusOfRadar = QWidgetHeight;
    }
    this->radiusOfRadar = this->radiusOfRadar / 2.0;

    this->centerOfTheRadar = new QPointF(QWidgetWidth/2.0, QWidgetHeight/2.0);
    this->spaceBetweenCirclesInRadar = (double)this->radiusOfRadar / (double)this->numberOfCirclesInRadar;
}

void Radar::setPainter(QPainter* painter){
    this->painter = painter;
}

void Radar::setPen(QColor color, Qt::PenStyle style, int width){
    if (this->pen == NULL){
        this->pen = new QPen();
    }
    this->pen->setColor(color);
    this->pen->setStyle(style);
    this->pen->setWidth(width);
    this->painter->setPen(*pen);
}

void Radar::setCenterOfTheRadar(QPointF* point){
    this->centerOfTheRadar = point;
}

void Radar::setNumberOfCirclesInRadar(int numberOfCirclesInRadar){
    this->numberOfCirclesInRadar = numberOfCirclesInRadar;
}

void Radar::setAngleLine(float angleOfLineToXAxis, float lengthOfLine){
    qreal rx = this->centerOfTheRadar->x();
    qreal ry = this->centerOfTheRadar->y();
    QPointF centerPoint(rx,ry);
    this->angleLine.setP1(QPointF(centerPoint));
    this->angleLine.setAngle(angleOfLineToXAxis);
    this->angleLine.setLength(lengthOfLine);
}

void Radar::setAngleLine(QLineF* angleLine, float angleOfLineToXAxis, float lengthOfLine){
    qreal rx = this->centerOfTheRadar->x();
    qreal ry = this->centerOfTheRadar->y();
    QPointF centerPoint(rx,ry);
    angleLine->setP1(QPointF(centerPoint));
    angleLine->setAngle(angleOfLineToXAxis);
    angleLine->setLength(lengthOfLine);
}
