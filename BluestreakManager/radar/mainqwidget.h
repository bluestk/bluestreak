#ifndef MAINQWIDGET_H
#define MAINQWIDGET_H


#include <QWidget>
#include "qwidgetwithradar.h"
#include "../navigation/robotnavigation.h"
#include <QBasicTimer>

class MainQWidget : public QWidget
{
    Q_OBJECT
public:
    MainQWidget(QWidget* parent =0);
    MainQWidget(RobotNavigation* robot_navigation, QWidget* parent =0);
    ~MainQWidget();
    void radarActions(int startPosition, int endPosition, int radarStep, int delayms);
public slots:
    void startTimer();
    void stopTimer();
private:
    void setUp(QWidget* parent =0);
    void  timerEvent(QTimerEvent* event) Q_DECL_OVERRIDE;
    std::string getStringFromSerialPort(char until = '\n', int timeout = 5000);
    bool checkStringFromSerialPort(std::string stringFromSerialPort);
    int getPositionOfSeparator(std::string stringFromSerialPort, char separator);
private:
    QWidgetWithRadar* qWidgetWithRadar;
    RobotNavigation* robot_navigation = NULL;
    QBasicTimer basicTimer;
};

#endif // MAINQWIDGET_H
