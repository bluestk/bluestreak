#include "qwidgetwithradar.h"

QWidgetWithRadar::QWidgetWithRadar(QWidget* parent) : QWidget(parent = 0){
    this->setWindowTitle("QWidget with Radar");
    this->resize(500,500);

    this->radar = new Radar(this->width(),this->height());
}

QWidgetWithRadar::~QWidgetWithRadar(){
    delete this->radar;
}

void QWidgetWithRadar::paintEvent(QPaintEvent * event){
    QPainter painter;
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing);
    this->radar->refreshRadarData(this->width(),this->height());
    this->radar->paint(&painter);
    //this->radar->drawAngleLineOnRadar();
    this->radar->drawListOfAngleLines();
    painter.end();
}

void QWidgetWithRadar::setAngleLineOnRadar(float angleOfLineToXAxis, float lengthOfLine){
    this->radar->setAngleLine(angleOfLineToXAxis, lengthOfLine);
}
