#include "mainqwidget.h"
#include <QLayout>
#include <QPushButton>
#include <QButtonGroup>

MainQWidget::MainQWidget(QWidget* parent) : QWidget(parent = 0){
    this->setUp(parent);
}

MainQWidget::MainQWidget(RobotNavigation* robot_navigation, QWidget* parent) : QWidget(parent = 0){
    this->setUp(parent);
    this->robot_navigation = robot_navigation;
}

void MainQWidget::setUp(QWidget* parent){
    this->qWidgetWithRadar =  new QWidgetWithRadar(this);
    this->setWindowTitle("main widget with radar");
    this->resize(500,500);

    QVBoxLayout *mainVerticalLayout = new QVBoxLayout;
    mainVerticalLayout->addWidget(this->qWidgetWithRadar);

    QHBoxLayout* subHorizontalLayout = new QHBoxLayout;
    QPushButton* buttonStart = new QPushButton();
    buttonStart->setText("Start");
    connect(buttonStart, SIGNAL(clicked(bool)), this, SLOT(startTimer()));
    subHorizontalLayout->addWidget(buttonStart);

    QPushButton* buttonStop = new QPushButton();
    buttonStop->setText("Stop");
    connect(buttonStop, SIGNAL(clicked(bool)), this, SLOT(stopTimer()));
    subHorizontalLayout->addWidget(buttonStop);


    mainVerticalLayout->addLayout(subHorizontalLayout);
    this->setLayout(mainVerticalLayout);
}


MainQWidget::~MainQWidget(){
    delete this->qWidgetWithRadar;
}

void MainQWidget::startTimer(){
    this->basicTimer.start(1,this);
    this->radarActions(0,180,1,20);
}

void MainQWidget::stopTimer(){
    this->basicTimer.stop();
}

void MainQWidget::radarActions(int startPosition, int endPosition, int radarStep, int delayms){
    std::string currentCommand = "R(" + std::to_string(startPosition) + "," + std::to_string(endPosition) + "," + std::to_string(radarStep) + "," + std::to_string(delayms) + ");";
    this->robot_navigation->writeString(currentCommand);
}

void  MainQWidget::timerEvent(QTimerEvent* event){
    if (event->timerId() == this->basicTimer.timerId()) {
        if(this->robot_navigation != NULL && this->robot_navigation->isConnected()){
            std::string stringFromSerialPort = getStringFromSerialPort('\n',200);
            if (this->checkStringFromSerialPort(stringFromSerialPort)){
                int positionOfSeparator = this->getPositionOfSeparator(stringFromSerialPort, '|');
                try{
                    float angleOfLineToXAxis = std::stof(stringFromSerialPort.substr(1,positionOfSeparator ));
                    float lenghtOfLine = std::stof(stringFromSerialPort.substr(positionOfSeparator +1,stringFromSerialPort.length()-1));
                    this->qWidgetWithRadar->setAngleLineOnRadar(angleOfLineToXAxis, lenghtOfLine);
                    this->qWidgetWithRadar->update();
                }catch(...){

                }
            }
        }
    }else{
        QWidget::timerEvent(event);
    }
}

std::string MainQWidget::getStringFromSerialPort(char until, int timeout){
    std::string stringFromSerialPort = this->robot_navigation->readString(until, timeout);
    return stringFromSerialPort;
}

bool MainQWidget::checkStringFromSerialPort(std::string stringFromSerialPort){
    if (stringFromSerialPort != ""){
        if ((stringFromSerialPort.at(0) == 'R') && (stringFromSerialPort.at(stringFromSerialPort.size()-3) == 'R')){
            return true;
        }
    }
    return false;
}

int MainQWidget::getPositionOfSeparator(std::string stringFromSerialPort, char separator){
    int positionOfSeparator = 0;
    while (stringFromSerialPort[positionOfSeparator] != separator && stringFromSerialPort.size()-1 > positionOfSeparator) {
        positionOfSeparator++;
    }
    if (positionOfSeparator == stringFromSerialPort.size()){
        positionOfSeparator = 0;
    }
    return positionOfSeparator;
}

