#include "mainwindow/mainwindow.h"
#include <QApplication>
#include <SDL2/SDL.h>

int main(int argc, char *argv[])
{
    SDL_InitSubSystem(SDL_INIT_JOYSTICK);
    SDL_JoystickEventState(SDL_IGNORE);
    QApplication a(argc, argv);
    QCoreApplication::setApplicationName("BluestreakManager" );
    QCoreApplication::setOrganizationName("Bluestreak");
    QCoreApplication::setOrganizationDomain("bluestreak.com");

    MainWindow w;
    w.show();


    return a.exec();
}
