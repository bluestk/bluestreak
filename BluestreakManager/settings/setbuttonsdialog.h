#ifndef SETBUTTONSDIALOG_H
#define SETBUTTONSDIALOG_H

#include <QDialog>
#include <QString>
#include <QSettings>
#include <QTimer>
#include <SDL2/SDL.h>

namespace Ui {
class SetButtonsDialog;
}

class SetButtonsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SetButtonsDialog(QWidget *parent = 0, QString button = "none");
    ~SetButtonsDialog();

private slots:
    void on_buttonBox_rejected();
    void update();

private:
    Ui::SetButtonsDialog *ui;
    QString button = "";
    QSettings *settings;
    QTimer *timer = NULL;
    SDL_Joystick * joy = NULL;
};

#endif // SETBUTTONSANDKEYSWIDGET_H
