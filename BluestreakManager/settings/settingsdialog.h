#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <QSettings>
#include "setbuttonsdialog.h"

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent = 0);
    ~SettingsDialog();

private slots:
    void on_buttonBox_accepted();

    void on_set_gear_up_button_clicked();

    void on_set_gear_down_button_clicked();

private:
    Ui::SettingsDialog *ui;
    QSettings *settings;

    void refresh_control();
};

#endif // SETTINGSDIALOG_H
