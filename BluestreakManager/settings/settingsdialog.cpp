#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include "../lib/serialconnection.h"

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
    settings = new QSettings();
    ui->serialport_edit->setText(settings->value("serial/port", SerialConnection::DEFAULT_COM_PORT).toString());
    ui->baud_rate_combobox->setCurrentText(settings->value("serial/baud_rate", SerialConnection::DEFAULT_BAUD_RATE).toString());

    refresh_control();
}

SettingsDialog::~SettingsDialog()
{
    delete settings;
    delete ui;
}

void SettingsDialog::on_buttonBox_accepted()
{
    settings->setValue("serial/port", ui->serialport_edit->text());
    settings->setValue("serial/baud_rate", ui->baud_rate_combobox->currentText());
}

void SettingsDialog::refresh_control()
{
    ui->gear_down_edit->setText(settings->value("controller/gear_down", 0).toString());
    ui->gear_up_edit->setText(settings->value("controller/gear_up", 0).toString());
}

void SettingsDialog::on_set_gear_up_button_clicked()
{
    SetButtonsDialog set_buttons_dialog(this,"gear_up");
    set_buttons_dialog.exec();
    refresh_control();
}

void SettingsDialog::on_set_gear_down_button_clicked()
{
    SetButtonsDialog set_buttons_dialog(this,"gear_down");
    set_buttons_dialog.exec();
    refresh_control();
}
