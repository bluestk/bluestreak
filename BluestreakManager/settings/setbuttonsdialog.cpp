#include "setbuttonsdialog.h"
#include "ui_setbuttonsdialog.h"

SetButtonsDialog::SetButtonsDialog(QWidget *parent, QString button) :
    QDialog(parent),
    ui(new Ui::SetButtonsDialog)
{
    ui->setupUi(this);
    ui->key_label->setText(button);
    this->button = button;
    settings = new QSettings();
    if (SDL_NumJoysticks() > 0) {
        joy = SDL_JoystickOpen(0);
        timer = new QTimer();
        connect(timer, SIGNAL(timeout()), this, SLOT(update()));
        timer->start(50);
    }
    this->setFocus();
}

SetButtonsDialog::~SetButtonsDialog()
{
    delete timer;
    delete settings;
    if (SDL_JoystickGetAttached(joy)) {
        SDL_JoystickClose(joy);
    }
    delete ui;
}

void SetButtonsDialog::on_buttonBox_rejected()
{
    SetButtonsDialog::close();
}

void SetButtonsDialog::update()
{
    SDL_JoystickUpdate();
    int num_of_buttons = SDL_JoystickNumButtons(joy);
    for(int i=0; i<num_of_buttons; i++) {
        if(SDL_JoystickGetButton(joy,i) == 1) {
            settings->setValue("controller/"+button, i);
            SetButtonsDialog::close();
        }
    }
}
