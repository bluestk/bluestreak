#-------------------------------------------------
#
# Project created by QtCreator 2015-10-21T19:32:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = bluestreak_manager
TEMPLATE = app
CONFIG += qt c++11

SOURCES += main.cpp \
    controller/controller.cpp \
    controller/controllerwidget.cpp \
    controller/touchmovement.cpp \
    lib/serialconnection.cpp \
    logger/logger.cpp \
    mainwindow/mainwindow.cpp \
    navigation/robotnavigation.cpp \
    radar/mainqwidget.cpp \
    radar/qwidgetwithradar.cpp \
    radar/radar.cpp \
    radar/serialmanager.cpp \
    recorder/action.cpp \
    recorder/actionmanager.cpp \
    recorder/movementplaybackdialog.cpp \
    settings/setbuttonsdialog.cpp \
    settings/settingsdialog.cpp \
    utils/stopwatch.cpp


HEADERS  += mainwindow/mainwindow.h \
    controller/controller.h \
    controller/controllerwidget.h \
    controller/touchmovement.h \
    lib/serialconnection.h \
    logger/logger.h \
    navigation/robotnavigation.h \
    radar/mainqwidget.h \
    radar/qwidgetwithradar.h \
    radar/radar.h \
    radar/serialmanager.h \
    recorder/action.h \
    recorder/actionmanager.h \
    recorder/movementplaybackdialog.h \
    settings/setbuttonsdialog.h \
    settings/settingsdialog.h \
    utils/stopwatch.h

win32 {
    HEADERS += lib/SerialClass.h
    SOURCES += lib/Serial.cpp
}
unix {
    HEADERS += lib/arduino-serial-lib.h
    SOURCES += lib/arduino-serial-lib.c
}

FORMS    += \
    controller/controllerwidget.ui \
    logger/logger.ui \
    mainwindow/mainwindow.ui \
    recorder/movementplaybackdialog.ui \
    settings/setbuttonsdialog.ui \
    settings/settingsdialog.ui

LIBS += -lSDL2main -lSDL2

QMAKE_CXXFLAGS += -std=c++11
