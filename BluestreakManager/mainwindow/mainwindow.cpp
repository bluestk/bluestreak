#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "../navigation/robotnavigation.h"
#include <QMessageBox>
#include <iostream>
#include "../settings/settingsdialog.h"
#include "../controller/controllerwidget.h"
#include "../recorder/movementplaybackdialog.h"

#include <cmath>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    toggleMovement(false);
    log = ui->logger;
    connect(ui->actionConnect, SIGNAL(triggered(bool)), this, SLOT(connectSlot()) );
    connect(ui->actionToggleLogger, SIGNAL(triggered(bool)), this, SLOT(toggleLogger()) );
    ui->touchMovement->setLogger( log );

    this->mainQWidgetWithRadar = NULL;
}

MainWindow::~MainWindow() {
    delete ui;
    delete this->mainQWidgetWithRadar;
}

void MainWindow::connectSlot() {
    QSettings settings;
    if(robot_navigation == NULL) {
        robot_navigation = new RobotNavigation(settings.value("serial/port", SerialConnection::DEFAULT_COM_PORT).toString().toStdString(), settings.value("serial/baud_rate", SerialConnection::DEFAULT_BAUD_RATE).toInt());
        if( !robot_navigation->isConnected() ){
            log->logError("Wystąpił błąd połączenia z robotem.");
            return;
        }

        QAction *caller = qobject_cast<QAction *>(sender());
        if (!caller) // caller is NULL if sender() cannot be cast to the requested type.
            return;
        caller->setText("Rozłącz");
        ui->controllerWidget->setRobotNavigation(robot_navigation);
        toggleMovement(true);
        connect(ui->touchMovement, SIGNAL(mouseMove(double,int)), robot_navigation, SLOT(move(double,int)) );
        connect(ui->touchMovement, SIGNAL(mouseMove(double,int)), robot_navigation, SLOT(move(double,int)) );
    } else if(robot_navigation ) {
        delete robot_navigation;
        robot_navigation = NULL;
        QAction *caller = qobject_cast<QAction *>(sender());
        if (!caller) // caller is NULL if sender() cannot be cast to the requested type.
            return;
        caller->setText("Połącz z robotem");
        toggleMovement(false);
    }
}

void MainWindow::on_b_forward_pressed() {
    log->logMovement("Move forward");

}

void MainWindow::on_b_right_pressed() {
    log->logMovement("Move right");

}

void MainWindow::on_b_left_pressed() {
    log->logMovement("Move left");

}

void MainWindow::on_b_backward_pressed() {
    log->logMovement("Move backward");

}


void MainWindow::on_b_forward_released() {

}

void MainWindow::on_b_right_released() {

}

void MainWindow::on_b_backward_released() {

}

void MainWindow::on_b_left_released() {

}

void MainWindow::toggleMovement(bool enabled) {
    ui->control->setVisible(enabled);
}

void MainWindow::toggleLogger() {
    if( ui->logger->isVisible() ) {
        QAction *caller = qobject_cast<QAction *>(sender());
        caller->setText("Pokaż logger");
        ui->logger->setVisible(false);
    } else {
        QAction *caller = qobject_cast<QAction *>(sender());
        caller->setText("Ukryj logger");
        ui->logger->setVisible(true);
    }
}

void MainWindow::on_actionSettings_triggered() {
    SettingsDialog settings_dialog;
    settings_dialog.setModal(true);
    settings_dialog.exec();
}

void MainWindow::on_actionRadar_triggered()
{
    if (this->robot_navigation != NULL){
        if (this->robot_navigation->isConnected()){
            if(this->mainQWidgetWithRadar == NULL){
                this->mainQWidgetWithRadar = new MainQWidget(this->robot_navigation);
                this->mainQWidgetWithRadar->show();
            }
        }
    }
}

void MainWindow::on_b_forward_left_pressed()
{

}

void MainWindow::on_b_forward_left_released()
{

}

void MainWindow::on_b_forward_right_pressed()
{

}

void MainWindow::on_b_forward_right_released()
{

}

void MainWindow::on_b_backward_left_pressed()
{

}

void MainWindow::on_b_backward_left_released()
{

}

void MainWindow::on_b_backward_right_pressed()
{

}

void MainWindow::on_b_backward_right_released()
{

}

void MainWindow::on_actionRecording_triggered()
{
    if(movementPlayback == NULL) {
        movementPlayback = new MovementPlaybackDialog();
        movementPlayback->setRobotNavigation(robot_navigation);
    }
    movementPlayback->show();
}
