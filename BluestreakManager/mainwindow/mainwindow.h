#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "../logger/logger.h"
#include "../navigation/robotnavigation.h"
#include "../controller/controllerwidget.h"
#include <QSettings>
#include "../recorder/movementplaybackdialog.h"
#include "../radar/mainqwidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void connectSlot();

    void on_b_forward_pressed();

    void on_b_right_pressed();

    void on_b_left_pressed();

    void on_b_backward_pressed();

    void on_b_forward_released();

    void on_b_right_released();

    void on_b_backward_released();

    void on_b_left_released();

    void on_actionSettings_triggered();

    void toggleLogger();

    void on_b_forward_left_pressed();

    void on_b_forward_left_released();

    void on_b_forward_right_pressed();

    void on_b_forward_right_released();

    void on_b_backward_left_pressed();

    void on_b_backward_left_released();

    void on_b_backward_right_pressed();

    void on_b_backward_right_released();

    void on_actionRecording_triggered();

    void on_actionRadar_triggered();

private:
    Logger* log;
    void toggleMovement(bool enabled);

    Ui::MainWindow *ui;
    RobotNavigation *robot_navigation = NULL;
    MovementPlaybackDialog *movementPlayback = NULL;
    MainQWidget* mainQWidgetWithRadar;
};

#endif // MAINWINDOW_H
