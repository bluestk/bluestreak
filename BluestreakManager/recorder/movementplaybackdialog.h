#ifndef MOVEMENTPLAYBACKDIALOG_H
#define MOVEMENTPLAYBACKDIALOG_H

#include <QDialog>
#include "../navigation/robotnavigation.h"
#include "actionmanager.h"
#include <QTimer>

namespace Ui {
class MovementPlaybackDialog;
}

class MovementPlaybackDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MovementPlaybackDialog(QWidget *parent = 0);
    ~MovementPlaybackDialog();
    void setRobotNavigation(RobotNavigation *robot_navigation);

private slots:
    void update();

    void on_recordButton_clicked();

    void on_playButton_clicked();

    void on_clearButton_clicked();

private:
    Ui::MovementPlaybackDialog *ui;
    ActionManager *actionManager = NULL;
    QTimer *timer = NULL;
    bool recording = false;
};

#endif // MOVEMENTPLAYBACKDIALOG_H
