#include "action.h"

Action::Action()
{

}

Action::Action(std::chrono::milliseconds delay, std::string command)
{
    this->delay = delay;
    this->command = command;
}

std::string Action::getCommand() const
{
    return command;
}

void Action::setCommand(const std::string &value)
{
    command = value;
}

std::chrono::milliseconds Action::getDelay() const
{
    return delay;
}

void Action::setDelay(const std::chrono::milliseconds &value)
{
    delay = value;
}

