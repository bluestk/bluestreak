#include "movementplaybackdialog.h"
#include "ui_movementplaybackdialog.h"

MovementPlaybackDialog::MovementPlaybackDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MovementPlaybackDialog)
{
    timer = new QTimer();
    ui->setupUi(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
}

MovementPlaybackDialog::~MovementPlaybackDialog()
{
    if(timer != NULL) {
        delete timer;
    }
    delete actionManager;
    delete ui;
}

void MovementPlaybackDialog::setRobotNavigation(RobotNavigation *robot_navigation)
{
    actionManager = new ActionManager(robot_navigation);
}

void MovementPlaybackDialog::update()
{
    actionManager->checkAndAddNewAction();
}

void MovementPlaybackDialog::on_recordButton_clicked()
{
    if(!recording) {
        timer->start(50);
        ui->recordButton->setText("Stop Recording");
        recording = true;
    } else {
        timer->stop();
        ui->recordButton->setText("Start Recording");
        recording = false;
    }

}

void MovementPlaybackDialog::on_playButton_clicked()
{
    actionManager->playbackActions();
}

void MovementPlaybackDialog::on_clearButton_clicked()
{
    actionManager->clearCommandList();
}
