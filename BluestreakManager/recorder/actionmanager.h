#ifndef ACTIONMANAGER_H
#define ACTIONMANAGER_H

#include "../navigation/robotnavigation.h"
#include "../utils/stopwatch.h"
#include "action.h"
#include <list>
#include <chrono>


class ActionManager
{
private:
    ActionManager();
    RobotNavigation *robot_navigation = NULL;
    std::list< Action > commandList;
    Stopwatch stopwatch;
public:
    ActionManager(RobotNavigation *robot_navigation);
    void checkAndAddNewAction();
    void playbackActions();
    void clearCommandList();
};

#endif // ACTIONMANAGER_H
