#ifndef ACTION_H
#define ACTION_H

#include <chrono>
#include <string>

class Action
{
private:
    Action();
    std::chrono::milliseconds delay;
    std::string command;
public:
    Action(std::chrono::milliseconds delay, std::string command);

    std::chrono::milliseconds getDelay() const;
    void setDelay(const std::chrono::milliseconds &value);
    std::string getCommand() const;
    void setCommand(const std::string &value);
};

#endif // ACTION_H
