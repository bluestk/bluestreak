#include "actionmanager.h"
#include <chrono>
#include <thread>

ActionManager::ActionManager()
{

}

ActionManager::ActionManager(RobotNavigation *robot_navigation)
{
    this->robot_navigation = robot_navigation;
}

void ActionManager::checkAndAddNewAction()
{
    if(commandList.empty()) {
        stopwatch.start();
        Action new_action(stopwatch.elapsed(),robot_navigation->getLastMovementCommand());
        commandList.push_back(new_action);
    } else if(commandList.back().getCommand() != robot_navigation->getLastMovementCommand()) {
        stopwatch.stop();
        Action new_action(stopwatch.elapsed(),robot_navigation->getLastMovementCommand());
        commandList.push_back(new_action);
        stopwatch.start();
    }
}

void ActionManager::playbackActions()
{
    for (std::list<Action>::iterator it=commandList.begin(); it != commandList.end(); ++it) {
        std::this_thread::sleep_for((*it).getDelay());
        robot_navigation->sendCommand((*it).getCommand());
    }
}

void ActionManager::clearCommandList()
{
    commandList.clear();
}

