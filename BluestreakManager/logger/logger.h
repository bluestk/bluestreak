#ifndef LOGGER_H
#define LOGGER_H

#include <QWidget>
#include <vector>

namespace Ui {
class Logger;
}

using namespace std;

class Logger : public QWidget
{
    Q_OBJECT

public:
    explicit Logger(QWidget *parent = 0);
    ~Logger();

    void clear();
    void logError(string msg);
    void logMovement(string msg);
    void logInfo(string msg);

private slots:
    void on_cbShowErrors_toggled(bool checked);

    void on_cbShowMovement_toggled(bool checked);

    void on_cbShowOther_toggled(bool checked);

private:
    Ui::Logger *ui;
    int loggerLevel = 0b111;


    enum logger_type {
        LOG_ERROR = 0b1,
        LOG_MOV = 0b10,
        LOG_OTHER = 0b100
    };

    struct message {
        logger_type type;
        string message;
    };

    vector<message> messageList;
    void refresh();
};

#endif // LOGGER_H
