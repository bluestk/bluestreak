#include "logger.h"
#include "ui_logger.h"

using namespace std;

Logger::Logger(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Logger)
{
    ui->setupUi(this);
}

Logger::~Logger()
{
    delete ui;
}

void Logger::logError(string msgText) {
    QString qtext = QString("<p style='color:\"#b70000\"'>");
    qtext.append(msgText.c_str());
    qtext.append("</p>");

    message msg;
    msg.type = LOG_ERROR;
    msg.message = qtext.toStdString();
    messageList.push_back(msg);
    if( (LOG_ERROR & loggerLevel) == LOG_ERROR )
        ui->textBrowser->append(qtext);
}

void Logger::logMovement(string msgText) {
    QString qtext = QString("<p style='color:\"#0000b7\"'>");
    qtext.append(msgText.c_str());
    qtext.append("</p>");

    message msg;
    msg.type = LOG_MOV;
    msg.message = qtext.toStdString();
    messageList.push_back(msg);
    if( (LOG_MOV & loggerLevel) == LOG_MOV )
        ui->textBrowser->append(qtext);
}

void Logger::logInfo(string msgText) {
    QString qtext = QString("<p>");
    qtext.append(msgText.c_str());
    qtext.append("</p>");

    message msg;
    msg.type = LOG_MOV;
    msg.message = qtext.toStdString();
    messageList.push_back(msg);

    if( (LOG_OTHER & loggerLevel) != LOG_OTHER )
        ui->textBrowser->append(qtext);
}

void Logger::refresh() {
    ui->textBrowser->clear();
    int msgCount = messageList.size();
    for( int i = 0;  i < msgCount; i++ ) {
        if( (messageList.at(i).type | loggerLevel) != loggerLevel )
            continue;
        ui->textBrowser->append(messageList.at(i).message.c_str());
    }
}

void Logger::on_cbShowErrors_toggled(bool checked)
{
    loggerLevel = checked ? loggerLevel | LOG_ERROR : loggerLevel ^ LOG_ERROR;
    refresh();
}

void Logger::on_cbShowMovement_toggled(bool checked)
{
    loggerLevel = checked ? loggerLevel | LOG_MOV : loggerLevel ^ LOG_MOV;
    refresh();
}

void Logger::on_cbShowOther_toggled(bool checked)
{
    loggerLevel = checked ? loggerLevel | LOG_OTHER : loggerLevel ^ LOG_OTHER;
    refresh();
}

