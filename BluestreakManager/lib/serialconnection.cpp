#include "serialconnection.h"
#include <iostream>

#ifdef _WIN32
   #include "SerialClass.h"
#else
    extern "C" {
        #include "arduino-serial-lib.h"
    }
#endif


SerialConnection::SerialConnection() : SerialConnection(DEFAULT_COM_PORT, DEFAULT_BAUD_RATE)
{

}

SerialConnection::SerialConnection(const std::string serialport, int baud)
{
#ifdef _WIN32
    SP = new Serial(serialport.c_str());
#else
    file_descriptor = serialport_init(serialport.c_str(), baud);
#endif
}

void SerialConnection::write_string(const std::string str)
{
#ifdef DEBUG
 return;
#endif

#ifdef _WIN32
    int length;
    length = strlen(str.c_str());
    SP -> WriteData(str.c_str(), length);
#else
    serialport_write(file_descriptor, str.c_str());
#endif
}

std::string SerialConnection::read_string(char until, int timeout)
{
#ifdef DEBUG
    return "DEBUG";
#endif

#ifdef _WIN32
    SP -> ReadData(buf, BUF_MAX);
#else
    serialport_read_until(file_descriptor, buf, until, BUF_MAX, timeout);
#endif
    return buf;
}

SerialConnection::~SerialConnection()
{
#ifdef _WIN32
   delete SP;
#else
   serialport_close(file_descriptor);
#endif
}

bool SerialConnection::isConnected() {
#ifdef DEBUG
    return true;
#endif

#ifdef _WIN32
    if( SP == NULL )
        return false;
    return SP->IsConnected();
#else
    return file_descriptor != -1;
#endif
}
