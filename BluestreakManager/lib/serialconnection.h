#ifndef SERIALCONNECTION_H
#define SERIALCONNECTION_H

#include <string>

class SerialConnection
{
private:
    int file_descriptor = -1;
    static constexpr const int BUF_MAX = 256;
    char buf[BUF_MAX];

#ifdef _WIN32
    Serial* SP;
#endif

public:
    static constexpr const int DEFAULT_BAUD_RATE = 9600;
#ifdef _WIN32
    static constexpr const char* DEFAULT_COM_PORT = "COM3";
    Serial* SP;
#else
    static constexpr const char* DEFAULT_COM_PORT = "/dev/ttyACM0";
#endif

    SerialConnection();
    SerialConnection(const std::string serialport, int baud);
    void write_string(const std::string str);
    std::string read_string(char until = '\n', int timeout = 5000);
    bool isConnected();
    ~SerialConnection();
};

#endif // SERIALCONNECTION_H
