#ifndef ROBOTNAVIGATION_H
#define ROBOTNAVIGATION_H
#include <QObject>
#include <string>

#include "../lib/serialconnection.h"

class RobotNavigation : public QObject
{
    Q_OBJECT
private:
    SerialConnection* serial_connection = NULL;
    std::string last_movement_command = "";

    int minLeftAcceleration = 0;
    int maxLeftAcceleration = 255;
    int minRightAcceleration = 0;
    int maxRightAcceleration = 255;

public:

    RobotNavigation();
    RobotNavigation(const std::string serialport, int baud);
    ~RobotNavigation();
    bool isConnected() { return serial_connection->isConnected(); }
    void sendMovementCommand(bool left_wheel_direction, uint8_t left_wheel_power, bool right_wheel_direction, uint8_t right_wheel_power);
    std::string getLastMovementCommand();
    void sendCommand(std::string command);
    std::string readString(char until = '\n', int timeout = 5000);
    void writeString(std::string command);

public slots:
    void move(double angle, int power);
    void mousePressChange(bool toggle);

};

#endif // ROBOTNAVIGATION_H
