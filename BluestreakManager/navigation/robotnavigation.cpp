#include "robotnavigation.h"
#include "../lib/serialconnection.h"
#include <iostream>
#include <chrono>
#include <thread>
#include <sstream>
#include <cmath>

RobotNavigation::RobotNavigation()
{
    serial_connection = new SerialConnection();
}

RobotNavigation::RobotNavigation(const std::string serialport, int baud)
{
    serial_connection = new SerialConnection(serialport, baud);
}

RobotNavigation::~RobotNavigation()
{
    delete serial_connection;
}

void RobotNavigation::sendMovementCommand(bool left_wheel_direction, uint8_t left_wheel_power, bool right_wheel_direction, uint8_t right_wheel_power)
{
    std::string current_command = "M(" + std::to_string(left_wheel_direction) + "," + std::to_string(left_wheel_power) + "," + std::to_string(right_wheel_direction) + "," + std::to_string(right_wheel_power) + ");";
    if(last_movement_command != current_command) {
        serial_connection->write_string(current_command);
        last_movement_command = current_command;
    }
}

std::string RobotNavigation::getLastMovementCommand()
{
    return last_movement_command;
}

void RobotNavigation::sendCommand(std::string command)
{
    serial_connection->write_string(command);
}

void RobotNavigation::mousePressChange(bool toggle) {
    if( !toggle )
        //stop();
    std::cerr << "ROBOT STOP" << std::endl;
}

void RobotNavigation::move(double angle, int power) {
    if( serial_connection == NULL )
        return;

    std::stringstream s;
    double speed, ratio;
    s << "M(";

    if( angle < 80  ) {
        ratio = angle / 80.0;
        s << "1,";
        speed = (maxLeftAcceleration - minLeftAcceleration) * power;
        speed /= 100;
        s << std::to_string( (int)speed );
        s << ",1,";
        speed = (maxRightAcceleration - minRightAcceleration) * power;
        speed /= 100;
        speed *= ratio;
        s << std::to_string( (int)speed );
    } else if ( angle < 100 ) { // Prosto do przodu
        s << "1,";
        speed = (maxLeftAcceleration - minLeftAcceleration) * power;
        speed /= 100;
        s << std::to_string( (int)speed );
        s << ",1,";
        speed = (maxRightAcceleration - minRightAcceleration) * power;
        speed /= 100;
        s << std::to_string( (int)speed );
    } else if ( angle < 180 ) {
        ratio = (angle - 100) / 80.0;
        s << "1,";
        speed = (maxLeftAcceleration - minLeftAcceleration) * power;
        speed /= 100;
        speed *= ratio;
        speed = maxLeftAcceleration - speed;
        s << std::to_string( (int)speed );
        s << ",1,";
        speed = (maxRightAcceleration - minRightAcceleration) * power;
        speed /= 100;
        s << std::to_string( (int)speed );
    } else if ( angle < 260 ) {
        ratio = ( angle - 180 ) / 80.0;
        s << "0,";
        speed = (maxLeftAcceleration - minLeftAcceleration) * power;
        speed /= 100;
        speed *= ratio;
        s << std::to_string( (int)speed );
        s << ",0,";
        speed = (maxRightAcceleration - minRightAcceleration) * power;
        speed /= 100;
        s << std::to_string( (int)speed );
    } else if ( angle < 280 ) { // Prosto do tyłu
        s << "0,";
        speed = (maxLeftAcceleration - minLeftAcceleration) * power;
        speed /= 100;
        s << std::to_string( (int)speed );
        s << ",0,";
        speed = (maxRightAcceleration - minRightAcceleration) * power;
        speed /= 100;
        s << std::to_string( (int)speed );
    } else {
        ratio = ( angle - 280 ) / 80.0;
        s << "0,";
        speed = (maxLeftAcceleration - minLeftAcceleration) * power;
        speed /= 100;
        s << std::to_string( (int)speed );
        s << ",0,";
        speed = (maxRightAcceleration - minRightAcceleration) * power;
        speed /= 100;
        speed *= ratio;
        speed = maxRightAcceleration - speed;
        s << std::to_string( (int)speed );
    }


    s << ");";
    std::string out;
    s >> out;

    std::cerr << "Wrzucam na seriala: " << out << std::endl;
    serial_connection->write_string(out);
}

std::string RobotNavigation::readString(char until, int timeout){
    std::string returnString = "";
    if (this->isConnected()){
        returnString = this->serial_connection->read_string(until,timeout);
    }
    return returnString;
}

void RobotNavigation::writeString(std::string command){
    serial_connection->write_string(command);
}

